#include "devices.h"

Device _devices; //lamps and air conditioners
Sensor _sensors; //all sensors

void initDevicesAndSensors() {

  Device initialDeviceValues = {0};
  Sensor initialSensorValues = {0};
  //initializing everything to 0, to not mess the menu
  _devices = initialDeviceValues;
  _sensors = initialSensorValues;

  //getting current info regarding BME and sensors
  Data data = getCurrentData();

  //the devices are set only through central control
  //so we keep the zeros
  data.devices = _devices;

  //print initial state to ncurses menu
  printDeviceToMenu(data);
  printSensorToMenu(_sensors);
}

int smoke(){
  // checks to see if there is any active smoke sensor
  return (_sensors.smoke1 || _sensors.smoket);
}


int anyActiveSensor(){


  if(smoke())return 1;

  //returns true if there's any active sensor
  int alarmChecks[] = {
    _sensors.prest, _sensors.wint01, _sensors.wint02, _sensors.doort,
    _sensors.pres1, _sensors.win101, _sensors.win102
  };

  for (size_t i = 0; i < 7; i++){
    if(alarmChecks[i])return 1;
  }
  return 0;
}

void sensorsHandler(int command) {
  //handles changes on sensor states
  //if a change was detected, invert the sensor

  switch (command)
  {
    case 1:
      _sensors.pres1 = !_sensors.pres1;
      break;
    case 2:
      _sensors.smoke1 = !_sensors.smoke1;
      break;
    case 3:
      _sensors.win101 = !_sensors.win101;
      break;
    case 4:
      _sensors.win102 = !_sensors.win102;
      break;
    case 5:
      _sensors.prest = !_sensors.prest;
      break;
    case 6:
      _sensors.smoket = !_sensors.smoket;
      break;
    case 7:
      _sensors.wint01 = !_sensors.wint01;
      break;
    case 8:
     _sensors.wint02 = !_sensors.wint02;
      break;
    case 9:
      _sensors.doort = !_sensors.doort;
      break;
    case 10:
      break;
    case 11:
      break;
    default:
      break;
  }

  //if there's any active sensor, plays the alarm
  if (anyActiveSensor()) {
    createAlarm();
  } else {

    //otherwise, the alarm should be off
    Data data = getCurrentData();
    _devices.playing = 0;
    setAlarmOff();
    data.devices = _devices;

    //updates devices in ncurses menu, as the alarm is one of them
    printDeviceToMenu(data);
  }

  //printing sensors to ncurses menu
  printSensorToMenu(_sensors);
}

void sendDeviceUpdate(int device,int new_state, int old_state, int server){

  unsigned short int port = server ? 10209 : 10109;

  if (new_state != old_state) {
    sendCommands(device, new_state, port);
  }
}

void updateDevices(Device devices) {

  //if the user chooses to deactivate the alarm
  if (!devices.alarm && devices.playing) {
    devices.playing = 0;
    setAlarmOff();
  }

  //if there is smoke, we want to activate water sprinkles
  if(smoke()){
    sendDeviceUpdate(ASPT,1,_devices.aspt,0);
  }
  //if the user has chosen to activate the alarm and there's any sensor active
  if (devices.alarm  && anyActiveSensor()) {
    devices.playing = 1;
  }
  
  //getting current device information
  //along with new user input
  Data data = getCurrentData();
  data.devices = devices;
  printDeviceToMenu(data);

  //if the user has chosen to change any device
  //then we send that requisition to the distributed server
  //responsible for making the change

  sendDeviceUpdate(LAMPT01,devices.lampt01,_devices.lampt01,0);
  sendDeviceUpdate(LAMPT02,devices.lampt02,_devices.lampt02,0);
  sendDeviceUpdate(LAMPCT,devices.lampct,_devices.lampct,0);
  sendDeviceUpdate(ART,devices.art,_devices.art,0);
  sendDeviceUpdate(LAMP101,devices.lamp101,_devices.lamp101,1);
  sendDeviceUpdate(LAMP102,devices.lamp102,_devices.lamp102,1);
  sendDeviceUpdate(LAMPC1,devices.lampc1,_devices.lampc1,1);
  sendDeviceUpdate(AR1,devices.ar1,_devices.ar1,1);

  _devices = devices;
  
}

Device getDevices() {
  //get current device states
  return _devices;
}
