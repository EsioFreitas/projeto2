#ifndef MENU_H_
#define MENU_H_

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <pthread.h>
#include "helpers.h"
#include "quit.h"
#include "devices.h"
#include "log.h"
#include "server.h"

//menu size
#define WIDTH 70
#define HEIGHT 30

void* optionsHandler(); //displays options menu
void printDeviceToMenu(Data data); //displays output devices info to menu
void printSensorToMenu(Sensor sensors); //displays sensors info to menu
void printDHT22(DHT22 data); //displays temperature and humidity info to menu

#endif /* MENU_H_ */