#include "dataRetrieval.h"

//used to keep data regarding the DHT22 info and devices info
Data received_data;

void dataInit() {
  //initializing data with zeros
  received_data.dht22.temperature = 0;
  received_data.dht22.humidity = 0;
}

Data getCurrentData() {
  //returns the current data
  return received_data;
}

void* updateData() {
  /*
    At each second, updates new DHT22 received info
    to menu
  */

  while (1) {
    // request new DHT22 info
    DHT22 dht22 = readDHTInfo();

    //print to menu
    printDHT22(dht22);

    // 1 second
    usleep(1000000);
  }

  return NULL;
}
