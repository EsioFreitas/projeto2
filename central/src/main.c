#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <dataRetrieval.h>
#include "quit.h"
#include "menu.h"
#include "devices.h"
#include "server.h"
#include "log.h"

int main() {

	//Attaching Ctrl+C to quit handler
	signal(SIGINT, quit);

	//creating logFILE
	createCSV();

	//initializing informations to be printed
	dataInit();

	pthread_t dataRetrieval, menu, server;

	//thread to request new temperature and humidity information
	pthread_create(&dataRetrieval, NULL, updateData, NULL);

	//thread to receive sensor updates from distributed servers
	pthread_create(&server, NULL, receiveCommands, NULL);

	//thread to display ncurses menu to terminal
	pthread_create(&menu, NULL, optionsHandler, NULL);

	pthread_join(dataRetrieval, NULL);
	pthread_join(menu, NULL);
	pthread_join(server, NULL);

	return 0;
}
