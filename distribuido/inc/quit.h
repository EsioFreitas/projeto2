#ifndef QUIT_H_
#define QUIT_H_


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include "gpio.h"

void quit(); //shuts everything off and exits

#endif /* QUIT_H_ */
