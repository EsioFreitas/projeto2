#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "server.h"
#include "gpio.h"
#include "quit.h"
#include "dht22.h"
#include "initialize.h"


int main(int argc, char **argv) {

	//registering Ctrl+C to quit function
	signal(SIGINT,quit); 

	//initializing GPIO and dht22
	initGPIO();
	initDht22();
  
  //receiving json file from command line to setup devices
	FILE *fp;
	char buffer[2000];

	if (argc >= 2){
		fp = fopen(argv[1],"r");
		fread(buffer, 2000, 1, fp);
		fclose(fp);
		init_device_info_from_json(buffer);
	}
	else{
		printf("Erro, o arquivo de inicializacao precisa ser passado como parametro");
		return -1;
	}

	//setting off all devices, in case there were any on before the launch
	setAllOff();

	//port from json which our socket will bind to
	int *port = malloc(sizeof(*port));
	*port = getPort();

	//one thread for the server, which will communicate with the central
	//and another for the gpio, which will monitor and interact with all gpio devices/sensors
	pthread_t server, gpio;

	pthread_create(&server, NULL, receiveFromCentral, port);
	pthread_create(&gpio, NULL, handleGPIO, NULL);

	pthread_join(server, NULL);
	pthread_join(gpio, NULL);
	
	return 0;
}
