#ifndef DEVICES_H_
#define DEVICES_H_

#include "helpers.h"
#include "server.h"
#include "menu.h"
#include "dataRetrieval.h"
#include "alarm.h"

//Floor 0  codes
#define LAMPT01 4 //lamps
#define LAMPT02 17
#define LAMPCT 27
#define ART 7 //air conditioner
#define ASPT 16 //sprinkler 

//Floor 1  codes
#define LAMP101 22 //lamps
#define LAMP102 25
#define LAMPC1 8
#define AR1 12 //air conditioner

void initDevicesAndSensors(); //initializing devices and sensors information
void updateDevices(Device devices); //updates device states 
void sensorsHandler(int command); //handle sensor state chages
Device getDevices(); //get current device states

#endif /* DEVICES_H_ */