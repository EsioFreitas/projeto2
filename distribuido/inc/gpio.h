#ifndef GPIO_H_
#define GPIO_H_

#include <wiringPi.h>
#include <stdio.h>    
#include <softPwm.h>
#include <unistd.h>
#include "server.h"
#include "initialize.h"


void initGPIO(); //initialize gpio with wiringpi
void toggleDevice(int item, int status); //toggle device on and off
void setAllOff(); // shut off every device
void* handleGPIO(); //handle sensor state changes

#endif /* GPIO_H_ */