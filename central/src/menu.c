#include "menu.h"

WINDOW *window;

void showMenu(WINDOW *window, int highlight, char *options[], int n_options) {
	//shows options menu

	int x = 2, y = 2, i;

	box(window, 0, 0);
	for(i = 0; i < n_options; i++) {
		if (i == n_options - 1) {
			wattron(window, COLOR_PAIR(2));
		}
		if (highlight == i + 1) {
			wattron(window, A_REVERSE);
			mvwprintw(window, y, x, "%s", options[i]);
			wattroff(window, A_REVERSE);
		} else {
			mvwprintw(window, y, x, "%s", options[i]);
		}
		if (i == n_options - 1) {
			wattroff(window, COLOR_PAIR(2));
		}
		++y;
	}
	wrefresh(window);
}

void clsMenu(WINDOW *window_param) {
	//clear window

	wclear(window_param);
	wborder(window_param, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');  
	wrefresh(window_param);
	delwin(window_param); 
}


void printStateLine(WINDOW *window_param, int y, int x, int value){
	//print colored ON/OFF next to each option	

	int color = value == 1 ? 3 : 2;

	wattron(window_param, COLOR_PAIR(color));
	mvwprintw(window_param, y, x, value == 1 ? "ON " : "OFF");
	wattroff(window_param, COLOR_PAIR(color));
}


void printDeviceToMenu(Data data) {
	//left side of screen, with all output devices

	Device devices = data.devices;
	int deviceValues[] = {devices.lampt01, devices.lampt02, devices.lampct, devices.art, 
	devices.lamp101, devices.lamp102, devices.lampc1, devices.ar1, devices.alarm, devices.aspt};

	for (size_t i = 2; i <= 11; i++){
		printStateLine(window, i,19,deviceValues[i-2]);
	}

  int playing = data.devices.playing == 1;

	wattron(window, COLOR_PAIR(playing ? 3 : 2));
	mvwprintw(window, 16, 2, "ALARM ACTIVE: %s", playing  ? "YES " : "NO ");
	wattroff(window, COLOR_PAIR(playing  ? 3 : 2));

  wrefresh(window);
}

void printDHT22(DHT22 data){
	//bottom left of screen, with temperature and humidity data

	wattron(window, COLOR_PAIR(1));
  mvwprintw(window, 14, 2, "TEMPERATURE: %4.2f", data.temperature);
  mvwprintw(window, 15, 2, "HUMIDITY: %4.2f", data.humidity);
	wattroff(window, COLOR_PAIR(1));
	wrefresh(window);
}


void printSensorToMenu(Sensor sensors) {
	//right side of screen, with sensor states

	int sensorValues[] = {
    sensors.prest, sensors.smoket, sensors.wint01, sensors.wint02, sensors.doort,
    sensors.pres1, sensors.smoke1, sensors.win101, sensors.win102
  };

	const char * sensorLabels[] ={ 
		"Sensor de Presença Terreo:",
		"Sensor de Fumaça Terreo:",
		"Sensor de Janela T01:",
		"Sensor de Janela T02:",
		"Sensor de Porta Terreo:",
		"Sensor de Presença Andar 1:",
		"Sensor de Fumaça Andar 1:",
		"Sensor de Janela 101:",
		"Sensor de Janela 102"
	};

	for (size_t i = 2; i <= 10; i++){
		mvwprintw(window, i, 24, sensorLabels[i-2]);
		printStateLine(window, i,63,sensorValues[i-2]);
	}

  wrefresh(window);
}

void* optionsHandler() {
	//handles user chosen options

	//available options
	char *options[] = {
		"LAMP 01 TERREO: ",
		"LAMP 02 TERREO: ",
		"LAMP COR TERREO:",
		"AR CONDI TERREO:",
		"LAMP 101 ANDAR1:",
		"LAMP 102 ANDAR1:",
		"LAMP COR ANDAR1:",
		"AR CONDI ANDAR1:",
		"ALARME:",
		"ASPERSOR DE AGUA:",
		"EXIT",
	};

	int startx = 0;
	int starty = 0;
	int n_options = sizeof(options) / sizeof(char *);

	int highlight = 1;
	int option = 0;
	int c;

	initscr();
	start_color(); 
	clear();
	noecho();
	cbreak();
	curs_set(0);
	init_pair(1, COLOR_YELLOW, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
     
	window = newwin(HEIGHT, WIDTH, starty, startx);
	keypad(window, TRUE);
	refresh();

	initDevicesAndSensors();

	do {
		showMenu(window, highlight, options, n_options);
		c = wgetch(window);

		switch(c) {
			case KEY_UP: //key up from keyboard on menu
				if(highlight == 1)
					highlight = n_options;
				else if(highlight == 11)
					highlight = 9;
				else
					--highlight;
				break;
			case KEY_DOWN: //key down from keyboard on menu
				if(highlight == n_options)
					highlight = 1;
				else if(highlight == 9)
					highlight = 11;
				else
					++highlight;
				break;
			case 10:    
				option = highlight;

				Device devices = getDevices();

				//if the user has chosen to alter any device
				//we make the state change and log the info
				if (option == 1) {
					devices.lampt01 = !devices.lampt01;
					appentToLog("LAMPTO1",devices.lampt01);
				}
				if (option == 2) {
					devices.lampt02 = !devices.lampt02;
					appentToLog("LAMPT02",devices.lampt02);
				}
				if (option == 3) {
					devices.lampct = !devices.lampct;
					appentToLog("LAMPCT",devices.lampct);
				}
				if (option == 4) {
					devices.art = !devices.art;
					appentToLog("ART",devices.art);
				}
				if (option == 5) {
					devices.lamp101 = !devices.lamp101;
					appentToLog("LAMP101",devices.lamp101);
				}
				if (option == 6) {
					devices.lamp102 = !devices.lamp102;
					appentToLog("LAMP102",devices.lamp102);
				}
				if (option == 7) {
					devices.lampc1 = !devices.lampc1;
					appentToLog("LAMPC1",devices.lampc1);
				}
				if (option == 8) {
					devices.ar1 = !devices.ar1;
					appentToLog("AR1",devices.ar1);
				}
				if (option == 9) {
					devices.alarm = !devices.alarm;
					appentToLog("ALARM",devices.alarm);
				}
				updateDevices(devices);

				if (option == 11) {
					//EXIT option
					quit();
				}

				break;
			default:
				refresh();
				break;
		}
	} while(1);

	return NULL;
}
