
#ifndef __DHT22__
#define __DHT22__

#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define MAX_TIMINGS	85
#define DEBUG 0
#define WAIT_TIME 2000

int initDht22(); //sensor init
int read_dht_data(); //use sensor to get new temperature and humidity
float getTemperature(); //returns temperature info
float getHumidity(); //returns humidity info

#endif // __DHT22__