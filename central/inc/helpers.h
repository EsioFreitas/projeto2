#ifndef HELPERS_H_
#define HELPERS_H_

/* Helper header to define gobaly used data structures*/

typedef struct Device{
  //floor zero
  int lampt01;
  int lampt02;
  int lampct;
  int art;
  int aspt;

  //floor 1
  int lamp101;
  int lamp102;
  int lampc1;
  int ar1;

  //alarm control
  int alarm;
  int playing;
} Device; //devices and alarm

typedef struct Sensor{
  //floor 0
  int prest;
  int smoket;
  int wint01;
  int wint02;
  int doort;

  //people count floor 0
  int people_entrance;
  int people_exit;

  //floor 1
  int pres1;
  int smoke1;
  int win101;
  int win102;
} Sensor; //sensors

typedef struct {
  double temperature;
  double humidity;
} DHT22; //dht22 info

typedef struct {
  DHT22 dht22;
  Device devices;
} Data; //dht22 info and devices

#endif /* HELPERS_H_ */