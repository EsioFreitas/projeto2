#ifndef SERVER_H_
#define SERVER_H_

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include "helpers.h"
#include "quit.h"
#include "alarm.h"
#include "devices.h"

//ip and port communication info
#define SERVER_DISTRIBUTED_IP "192.168.0.22"
#define SERVER_CENTRAL_PORT 10009
#define SERVER_DISTRIBUTED_PORT 10109

void* receiveCommands(); //to receive sensor states from distributed
int sendCommands(int item, int status, unsigned short int port); //to send commands to distributed
DHT22 readDHTInfo(); //to read new DHT22 info

#endif /* SERVER_H_ */
