#ifndef INITIALIZE_H_
#define INITIALIZE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"


void init_device_info_from_json(const char * const devices_json); //reads json file to see what are the devices and sensors
int getNumberOfOutputDevices(); //number of output devices available from json
int *getOutputDevices(); //output devices available gpio ports
int getNumberOfInputDevices(); //number of input devices available from json
int *getInputDevices(); //input devices available gpio ports
char *getIP(); //get ip from json
int getPort(); //get port from json

#endif /* INITIALIZE_H_ */
