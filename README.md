# FSE - Projeto 2 - Automação Predial

O projeto visa construir um recurso de automação predial distribuida. Há um servidor central que apresenta um
menu para o usuário, no qual ele pode decidir quais dispositivos quer acionar, monitorar o estado dos sensores, bem como a temperatura e umidade do ambiente, e armar o alarme. Os servidores distribuidos por sua vez, tem por função controlar os dispositivos de saída de acordo com as requisições recebidas, e enviar os dados da temperatura e dos sensores ao servidor central.

### Aluno: Esio Gustavo Pereira Freitas - 170033066

## Execução

- **Linguagem:** C
- **Instruções de execução:**

1. Clonar o repositório em ambos os servidores, executando `git clone <url>`
2. Ir para o servidor central e navegar até o diretório `central`
3. Rodar os comandos `make` e `make run`
4. Ir para o servidor distribuido e navegar até o diretório `distribuido`
5. Rodar os comandos `make` e `make terreo`
6. Ir para uma nova instância do servidor distribuido e navegar até o diretório `distribuido`
7. Rodar os comandos `make` e `make primeiro`

- **Instruções de uso:**
  Assim que executado, aparecerá um menu de opções no servidor central. Basta utilizar o menu para interagir com os dispositivos e ver as informações sendo atualizadas na tela. Nas telas dos servidores distribuidos também pode ser visto o registro de eventos que eles estão recebidos. O servidor central também gera um arquivo log.csv com os registros de sua parte.

## Observação
As portas utilizadas para cada um dos servidores distribuidos podem ser customizadas através dos arquivos JSON. O ip e a porta do servidor principal devem ser customizados através das header files server.h, dentro de distribuido/inc e central/inc.

## Screenshots

![screen1](screenshots/screen1.png)
![screen2](screenshots/screen2.png)
